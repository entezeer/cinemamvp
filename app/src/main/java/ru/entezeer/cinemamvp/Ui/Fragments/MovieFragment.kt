package ru.entezeer.cinemamvp.Ui.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.entezeer.cinemamvp.Models.MovieModel
import ru.entezeer.cinemamvp.Models.MovieResult
import ru.entezeer.cinemamvp.R
import ru.entezeer.cinemamvp.Ui.Activities.SecondMovieActivity
import ru.entezeer.cinemamvp.Ui.Adapters.MovieAdapter
import ru.entezeer.cinemamvp.Utils.ApiClient
import ru.entezeer.cinemamvp.Utils.Requests.ApiMovie
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MovieFragment : Fragment(),MovieAdapter.Listener {
    var movieData: ArrayList<MovieResult> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_movie,container,false)
        var movieRecyclerView: RecyclerView =view.findViewById(R.id.movie_recycler) as RecyclerView
        movieRecyclerView.layoutManager = LinearLayoutManager(activity)
        movieRecyclerView.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        var apiMovie: ApiMovie = ApiClient().getApiClient()!!.create(ApiMovie::class.java)
        apiMovie.getMovies().enqueue(object: Callback<MovieModel> {
            override fun onResponse(call: Call<MovieModel>?, response: Response<MovieModel>?) {
                movieData = response?.body()!!.result
                movieRecyclerView.adapter = MovieAdapter(movieData, this@MovieFragment)
                Toast.makeText(activity,response?.body()!!.succes.toString(), Toast.LENGTH_LONG).show()
            }
            override fun onFailure(call: Call<MovieModel>?, t: Throwable?) {
                Toast.makeText(activity,"Error", Toast.LENGTH_LONG).show()
            }
        })
        movieRecyclerView.setOnClickListener { val intent = Intent(activity,SecondMovieActivity::class.java)
        startActivity(intent)
        }
        return view
    }

    override fun onItemSelectedAt(position: Int) {
            val intent=  Intent(activity, SecondMovieActivity::class.java)
            intent.putExtra("name",movieData.get(position).name)
            intent.putExtra("image",movieData.get(position).image)
            intent.putExtra("countries",movieData.get(position).countries)
            intent.putExtra("vote",movieData.get(position).vote)
            intent.putExtra("age_limit",movieData.get(position).age_limit)
            intent.putExtra("actors",movieData.get(position).actors)
            intent.putExtra("rejisser",movieData.get(position).rejisser)
            intent.putExtra("premier",movieData.get(position).premier_ua)
            startActivity(intent)
        }


}

