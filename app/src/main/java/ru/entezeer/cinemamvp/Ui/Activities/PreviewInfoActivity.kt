package ru.entezeer.cinemamvp.Ui.Activities

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_preview_info.*
import kotlinx.android.synthetic.main.activity_second_movie.*
import ru.entezeer.cinemamvp.R

class PreviewInfoActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview_info)
        var rejisser = intent.getStringExtra("rejisser")
        var actors = intent.getStringExtra("actors")
        rejisser= Html.fromHtml(rejisser).toString()
        actors= Html.fromHtml(actors).toString()
        var entered = intent.getStringExtra("entered")
        entered = Html.fromHtml(entered).toString()
        var imgUrl = intent.getStringExtra("image")
        imgUrl=imgUrl.replace("sm_","")
        previewName.text = intent.getStringExtra("name")
        Picasso.with(this).load("https://kinoafisha.ua/" +imgUrl).into(previewImage)
        informationOfPreview.text=getString(R.string.informationOfPreview,intent.getStringExtra("countries"),rejisser,actors,entered,intent.getStringExtra("before"))
    }
}
