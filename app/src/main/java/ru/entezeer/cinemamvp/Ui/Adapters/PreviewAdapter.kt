package ru.entezeer.cinemamvp.Ui.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import ru.entezeer.cinemamvp.Models.PreviewResult
import ru.entezeer.cinemamvp.R

class PreviewAdapter(val items: ArrayList<PreviewResult>?, val listener: Listener): RecyclerView.Adapter<PreviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.recycler_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(p0: ViewHolder, position: Int) {
        p0.bindData(items, position)
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val preview_name = itemView.findViewById<TextView>(R.id.name)
        val preview_info = itemView.findViewById<TextView>(R.id.info)
        val preview_image = itemView.findViewById<ImageView>(R.id.image)
        fun bindData(items: ArrayList<PreviewResult>?, position: Int) {
            preview_name.text = items!!.get(position).name
            preview_info.text = items!!.get(position).countries
            var imgUrl=items!!.get(position).image
//            imgUrl=imgUrl.replace("sm_","")
            Picasso.with(itemView.context).load("https://kinoafisha.ua/" + imgUrl).into(preview_image)
            itemView.tag = position
            itemView.setOnClickListener{v->
                val index = v.tag as Int
                listener.onItemSelectedAt(index)
            }
        }
    }
    interface Listener{
        fun onItemSelectedAt(position: Int)
    }
}