package ru.entezeer.cinemamvp.Ui.Adapters

import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import ru.entezeer.cinemamvp.Models.MovieResult
import ru.entezeer.cinemamvp.R
import ru.entezeer.cinemamvp.Ui.Activities.SecondMovieActivity

class MovieAdapter(val items: ArrayList<MovieResult>?, var listener:Listener): RecyclerView.Adapter<MovieAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent?.context).inflate(R.layout.recycler_list_item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(p0: ViewHolder, position: Int) {
        p0.bindData(items,position)

    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val movie_name = itemView.findViewById<TextView>(R.id.name)
        val movie_info = itemView.findViewById<TextView>(R.id.info)
        val movie_image = itemView.findViewById<ImageView>(R.id.image)

        fun bindData(items: ArrayList<MovieResult>?, position: Int){
            movie_name.text = items!!.get(position).name
            movie_info.text = items!!.get(position).countries
            var imgUrl=items!!.get(position).image
//            imgUrl=imgUrl.replace("sm_","")
            Picasso.with(itemView.context).load("https://kinoafisha.ua/" + imgUrl).into(movie_image)
            itemView.tag = position
            itemView.setOnClickListener{v->
                val index = v.tag as Int
                listener.onItemSelectedAt(index)
            }
        }
    }
    interface Listener{
        fun onItemSelectedAt(position: Int)
    }
}