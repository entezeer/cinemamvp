package ru.entezeer.cinemamvp.Ui.Activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import ru.entezeer.cinemamvp.Ui.Fragments.MovieFragment
import ru.entezeer.cinemamvp.Ui.Fragments.PreviewFragment
import ru.entezeer.cinemamvp.R
import ru.entezeer.cinemamvp.Ui.Fragments.InfoFragment

class MainActivity : AppCompatActivity() {

    lateinit var CINEMA_NAME: String
    private val bottomNavigationSelected = BottomNavigationView.OnNavigationItemSelectedListener {item->
        when(item.itemId){
            R.id.bottom_films ->{
                replaceFragment(MovieFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.bottom_anons ->{
                replaceFragment(PreviewFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.bottom_info->{
                replaceFragment(InfoFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount==1){
            finish()
        }
        else super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavigation = findViewById(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigation.setOnNavigationItemSelectedListener(bottomNavigationSelected)
        replaceFragment(MovieFragment())

    }
    private fun replaceFragment(fragment: android.support.v4.app.Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
