package ru.entezeer.cinemamvp.Ui.Activities

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_second_movie.*
import ru.entezeer.cinemamvp.R

class   SecondMovieActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_movie)
        var rejisser = intent.getStringExtra("rejisser")
        var actors = intent.getStringExtra("actors")
        rejisser=Html.fromHtml(rejisser).toString()
        actors=Html.fromHtml(actors).toString()
        var imgUrl = intent.getStringExtra("image")
        imgUrl=imgUrl.replace("sm_","")
        movieName.text = intent.getStringExtra("name")
        Picasso.with(this).load("https://kinoafisha.ua/" +imgUrl).into(movieImage)
        information.text=getString(R.string.information,intent.getStringExtra("countries"),rejisser,actors,intent.getStringExtra("premier"),intent.getStringExtra("vote"))

    }
}
