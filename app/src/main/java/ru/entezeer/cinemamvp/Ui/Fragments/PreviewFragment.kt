package ru.entezeer.cinemamvp.Ui.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.entezeer.cinemamvp.Models.MovieModel
import ru.entezeer.cinemamvp.Models.MovieResult
import ru.entezeer.cinemamvp.Models.PreviewModel
import ru.entezeer.cinemamvp.Models.PreviewResult
import ru.entezeer.cinemamvp.R
import ru.entezeer.cinemamvp.Ui.Activities.PreviewInfoActivity
import ru.entezeer.cinemamvp.Ui.Activities.SecondMovieActivity
import ru.entezeer.cinemamvp.Ui.Adapters.MovieAdapter
import ru.entezeer.cinemamvp.Ui.Adapters.PreviewAdapter
import ru.entezeer.cinemamvp.Utils.ApiClient
import ru.entezeer.cinemamvp.Utils.Requests.ApiMovie
import ru.entezeer.cinemamvp.Utils.Requests.ApiPreview


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PreviewFragment : Fragment(),PreviewAdapter.Listener {
    var previewData: ArrayList<PreviewResult> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_preview, container, false)
        var previewRecyclerView: RecyclerView =view.findViewById(R.id.preview_recycler) as RecyclerView
        previewRecyclerView.layoutManager = LinearLayoutManager(activity)
        previewRecyclerView.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        var apiPreview: ApiPreview = ApiClient().getApiClient()!!.create(ApiPreview::class.java)
        apiPreview.getPreviews().enqueue(object: Callback<PreviewModel> {
            override fun onResponse(call: Call<PreviewModel>?, response: Response<PreviewModel>?) {
                previewData = response?.body()!!.result
                previewRecyclerView.adapter = PreviewAdapter(previewData, this@PreviewFragment)
                Toast.makeText(activity,response?.body()!!.succes.toString(), Toast.LENGTH_LONG).show()
            }
            override fun onFailure(call: Call<PreviewModel>?, t: Throwable?) {
                Toast.makeText(activity,"Error", Toast.LENGTH_LONG).show()
            }
        })
    return view
    }
    override fun onItemSelectedAt(position: Int) {
        val intent=  Intent(activity, PreviewInfoActivity::class.java)
        intent.putExtra("name",previewData.get(position).name)
        intent.putExtra("image",previewData.get(position).image)
        intent.putExtra("countries",previewData.get(position).countries)
        intent.putExtra("entered",previewData.get(position).entered)
        intent.putExtra("before",previewData.get(position).before)
        intent.putExtra("actors",previewData.get(position).actors)
        intent.putExtra("rejisser",previewData.get(position).rejisser)
        startActivity(intent)
    }

}
