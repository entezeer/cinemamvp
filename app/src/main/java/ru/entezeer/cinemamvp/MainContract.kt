package ru.entezeer.cinemamvp

interface MainContract {
    fun showProgress()
    fun hideProgress()
    fun setData()
}