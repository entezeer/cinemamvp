package ru.entezeer.cinemamvp.Utils

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient{

        var BASE_URL = "http://kinoafisha.ua/ajax/"
        public var retrofit: Retrofit? = null
        public fun getApiClient(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create()).build()
            }
            return retrofit
        }
}


