package ru.entezeer.cinemamvp.Utils.Requests

import retrofit2.Call
import retrofit2.http.GET
import ru.entezeer.cinemamvp.Models.PreviewModel

interface ApiPreview {
    @GET("skoro_load")
  fun getPreviews(): Call<PreviewModel>
}