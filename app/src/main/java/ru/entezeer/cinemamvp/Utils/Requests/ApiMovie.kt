package ru.entezeer.cinemamvp.Utils.Requests

import retrofit2.Call
import retrofit2.http.GET
import ru.entezeer.cinemamvp.Models.MovieModel

interface ApiMovie {

    @GET("kinoafisha_load")
    abstract fun getMovies(): Call<MovieModel>
}